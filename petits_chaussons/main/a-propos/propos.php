<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../css/propos.css"/>
    <link rel="stylesheet" href="../css/footer.css">
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display&family=Roboto:wght@100&display=swap" rel="stylesheet">
    <title>A Propos</title>
</head>
<body>


    <div class="conteneur">
    
        <div id="bloc-logo">
            <img src="../assets/inscription-connexion/miellat-png.png" alt="logo"/>
        </div>
    
        <nav>
            <ul>
                <li><a href="../index.php">Accueil</a></li>
                <li><a href="../a-propos/propos.php">A propos</a></li>
            </ul>
        </nav>
    
        
        
        
          <h2>Petit Chausson</h2>
            <p>
                Bienvenue sur Petit Chausson !
                Nous vous proposons une méthode de travail qui s’inspire de l’approche Lean centrée sur l'amélioration continue des processus de production.
                Notre but consiste ainsi à s'adapter en permanence aux besoins du client. Objectif : limiter le risque de surproduction et de gaspillage, mais aussi réduire les délais et les coûts. Qualifiée de méthode agile au même titre que Scrum, Petit chausson prône la visualisation des flux de travail par le biais d'un tableau (dit tableau Kanban), permettant de prioriser et suivre l'état d'avancement des tâches à accomplir. 
                Installez confortablement vos petits petons et soyez productifs, de grands projets vous attendent dans vos petits chaussons. 
            </p>
        
        
<div id="wrapper-profils">
    <div id="profils">
        <h2>Notre Equipe</h2>


        <div id="profile-pic">
            <a href="https://www.linkedin.com/in/hugo-mortreux-342b62206/"><img id="Hugo" src="../assets/a-propos/hujo.jpeg" alt="Hugo"></a>
            <a href="https://www.linkedin.com/in/ulrich-ngouamba-6a744b1b5/"><img id="Ulrich" src="../assets/a-propos/ulrich.jpeg" alt="Ulrich"></a>
            <a href="https://www.linkedin.com/in/guillaume-gery-1a1827212/"><img id="Guillaume" src="../assets/a-propos/gui.jpeg" alt="Guillaume"></a>
            <a href="https://www.linkedin.com/in/leo-recouvreux/"><img id="Leo" src="../assets/a-propos/leo.jpeg" alt="Léo"></a>
            <a href="https://www.linkedin.com/in/julien-ferrara-1bb610173/"><img id="Julien" src="../assets/a-propos/ju.jpeg" alt="Julien"></a>
        </div>
    </div>
</div>

    </div>
       
        <?php
        include '../footer/footer.php';
        ?>

</body>
</html>