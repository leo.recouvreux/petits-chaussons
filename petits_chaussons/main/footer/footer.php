<footer >
    
    <img id="logo_footer" src="../assets/footer/logo_footer.png" alt="logo footer"/>
    <p class="copyright">
      Petit Chausson © 2021
    </p>
    <p class="contact">
        Guillaume Gery, Hugo Mortreux, Ulrich Ngouamba, Léo Recouvreux, Ferrara Julien.</br> Simplon PACA, Marseille 13013
    </p>
</footer>